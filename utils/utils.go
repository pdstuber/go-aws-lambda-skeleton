package utils

import (
	"context"
	"go-aws-lambda-skeleton/logging"
	"os"
	"os/signal"

	"go.uber.org/zap"
)

// CatchPosixSignals catches POSIX signals and invokes 'cancel' if a relevant
// signal is received. If 'ctxt' is canceled, the function just returns.
func CatchPosixSignals(ctxt context.Context, cancel context.CancelFunc) {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, os.Interrupt)
	defer signal.Stop(sigs)

	for {
		select {
		case sig := <-sigs:
			logging.Logger().Info(
				"received POSIX signal", zap.Stringer("signal", sig),
			)
			cancel()
		case <-ctxt.Done():
			return
		}
	}
}
