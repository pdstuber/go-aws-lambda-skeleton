package logging

import (
	"go.uber.org/zap"
)

var (
	logger = createLogger()
)

// Logger returns a configured zap logger instance
func Logger() *zap.Logger {
	return logger
}

func createLogger() *zap.Logger {
	cfg := zap.NewProductionConfig()
	logger, _ := cfg.Build()
	return logger
}
