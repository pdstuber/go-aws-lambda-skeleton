module go-aws-lambda-skeleton

go 1.16

require (
	github.com/aws/aws-lambda-go v1.23.0
	github.com/spf13/cobra v1.1.3
	go.uber.org/zap v1.16.0
)
