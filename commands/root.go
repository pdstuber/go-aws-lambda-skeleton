package commands

import (
	"context"
	"go-aws-lambda-skeleton/commands/greeter"
	"go-aws-lambda-skeleton/utils"

	"github.com/spf13/cobra"
)

// GetRoot creates and returns the root cobra command
func GetRoot(name string) *cobra.Command {
	root := &cobra.Command{
		Use:   name,
		Short: "main entry point for all lambda functions",
	}
	ctx, cancel := context.WithCancel(context.Background())
	go utils.CatchPosixSignals(ctx, cancel)

	root.AddCommand(greeter.GetCommand(ctx))
	return root
}
