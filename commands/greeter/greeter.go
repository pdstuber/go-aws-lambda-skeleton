package greeter

import (
	"context"
	"fmt"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/spf13/cobra"
)

// Event is the data structure for incoming requests
type Event struct {
	Name string `json:"name"`
}

// Greeting is the data structure this service responds with
type Greeting struct {
	Message string `json:"greeting"`
}

// GetCommand returns a cobra command for the greeter function
func GetCommand(ctx context.Context) *cobra.Command {
	var (
		root = &cobra.Command{
			Use:   "greeter",
			Short: "Point-of-entry to the greeter function",
			Run: func(cmd *cobra.Command, args []string) {
				lambda.Start(handleEvent)
			},
		}
	)

	return root
}

func handleEvent(ctx context.Context, event Event) (Greeting, error) {
	return Greeting{Message: fmt.Sprintf("Hello %s!", event.Name)}, nil
}
