.PHONY: tools lint unit-tests

all: tools lint unit-tests

unit-tests:
	@go test -v ./... -cover -race

tools:
	@pushd "${GOPATH}" > /dev/null; \
	go get golang.org/x/lint/golint; \
	go get golang.org/x/tools/cmd/goimports; \
	popd > /dev/null

lint: tools
	@golint ./...


fmt:
	@gofmt -l -w .

imports: tools
	@goimports -l -w .

build:
	@rm -rf target; \
	mkdir target; \
	go mod download; \
	CGO_ENABLED=0 go build -o target/main; \

bundle: build
	zip -j target/handler.zip target/main

invoke: build bundle
	@sam local invoke -t deploy/sam_template.yml


