package main

import (
	"go-aws-lambda-skeleton/commands"
	"go-aws-lambda-skeleton/logging"
	"os"

	"go.uber.org/zap"
)

func main() {
	if err := commands.GetRoot(os.Args[0]).Execute(); err != nil {
		logging.Logger().Fatal("failed to execute", zap.Error(err))
	}
}
